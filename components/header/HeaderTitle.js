import React, {Component} from 'react';
import { View, Text } from 'react-native';
import computeProps from '../../utils/computeProps';
import SCALE from '../../utils/scale';
import COLOR from '../../utils/color';
import _ from 'lodash';

export default class HeaderTitle extends Component {

    propTypes: {
        style : React.PropTypes.object,
		allowFontScaling : React.PropTypes.boolean
    }

    getInitialStyle() {
        return {
            headerTitleGrp:{
                alignItems      : 'center',
                justifyContent  : 'center',
                position        : 'absolute',
                width           : SCALE.DEVICE_WIDTH,
                height          : SCALE.RATIO_Y*(SCALE.PLATFORM_OS == 'ios'?60:50),
                top             : SCALE.PLATFORM_OS == 'ios'?SCALE.RATIO_Y*8:0,
                left            : 0,
            },
            headerTitle:{
                color       : COLOR.COLOR_WHITE,
                fontSize    : SCALE.FONT_SIZE_14,
                fontFamily  : 'Futura',
            },
        }
    }

    prepareRootProps() {
        var defaultProps = {
            style: this.getInitialStyle().headerTitleGrp,
        };
        return computeProps(this.props,defaultProps);

    }
    prepareTextProps() {
        var defaultProps = {
            style: this.getInitialStyle().headerTitle,
        };
        return computeProps({style:this.props.textStyle}, defaultProps);

    }
    renderChildren() {
        if(typeof this.props.children == 'string') {
			return <Text {...this.prepareTextProps()} allowFontScaling={this.props.allowFontScaling || false}>{this.props.children}</Text>;
		}else if(Array.isArray(this.props.children)) {
			var newChildren = [];
			React.Children.forEach(this.props.children, (child,i) => {
				if(typeof child == 'string') {
					newChildren.push(<Text key={'_text_'+i} {...this.prepareTextProps()} allowFontScaling={this.props.allowFontScaling || false}>{child}</Text>);
				}else{
					newChildren.push(child);
				}
	        });
			return newChildren;
		}
    }
    render() {
        return(
            <View {...this.prepareRootProps()} >
                {this.renderChildren()}
            </View>
        );
    }
}
